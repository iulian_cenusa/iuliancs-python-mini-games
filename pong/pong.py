#Python pong game
#Autor: Iulian Cenusa
#Tutorial: https://www.youtube.com/watch?v=XGf2GcyHPhc
#Credits: https://www.freecodecamp.org/
#-----------------------
#Import part
#-----------------------
import turtle
#-----------------------
#Define workspace
#-----------------------
window = turtle.Screen()
window.title('Pong by @IulianC')
window.bgcolor('lightgray')
window.setup(width=800,height=600)
window.tracer(0)
#-----------------------
#Add game objects
#-----------------------
#---player1 - red---
player1 = turtle.Turtle()
player1.speed(0)
player1.shape('square')
player1.color('red')
player1.shapesize(stretch_wid=5,stretch_len=1)
player1.penup()
player1.goto(-350,0)
#---player2 - blue---
player2 = turtle.Turtle()
player2.speed(0)
player2.shape('square')
player2.color('blue')
player2.shapesize(stretch_wid=5,stretch_len=1)
player2.penup()
player2.goto(350,0)
#---ball - neutral---
ball = turtle.Turtle()
ball.speed(0)
ball.shape('square')
ball.color('black')
ball.penup()
ball.goto(0,0)
ball.dx = 2
ball.dy = 2
#---Score---
p1_score = 0
p2_score = 0
pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0,260)
pen.write("Player1: "+ str(p1_score) +"  Player2: "+ str(p2_score),align="center",font=("Courier",24,"normal"))
#-----------------------
#Game logic - movement of players
#-----------------------
speed = 20 #variable to be used for speed of players
#one confgiration for all
def p1_up():
    y = player1.ycor() # actual y pos of red
    y += speed # add px to y pos
    player1.sety(y)
def p1_down():
    y = player1.ycor() # actual y pos of red
    y -= speed # substracts px to y pos
    player1.sety(y)
def p2_up():
    y = player2.ycor() # actual y pos of red
    y += speed # add px to y pos
    player2.sety(y)
def p2_down():
    y = player2.ycor() # actual y pos of red
    y -= speed # substracts px to y pos
    player2.sety(y)
def ext():
    window.exitonclick()
#-----------------------
#Game logic - keyboard listening
#-----------------------
window.listen()
window.onkeypress(p1_up,'a')
window.onkeypress(p1_down,'z')
window.onkeypress(p2_up,'k')
window.onkeypress(p2_down,'m')
window.onkeypress(ext,'t')
#-----------------------
#Main loop
#-----------------------
while True:
    window.update()

    #ball moving
    ball.setx(ball.xcor() + ball.dx/15)
    ball.sety(ball.ycor() + ball.dy/15)

    #---Border checks---

    #uppder border
    if ball.ycor() > 290:
        ball.sety(290)
        ball.dy *=-1
    #lower border
    if ball.ycor() < -290:
        ball.sety(-290)
        ball.dy *=-1
    #left border
    if ball.xcor() > 390:
        ball.goto(0,0)
        ball.dx *=-1
        p1_score = p1_score + 1     #score
        pen.clear()
        pen.write("Player1: "+ str(p1_score) +"  Player2: "+ str(p2_score),align="center",font=("Courier",24,"normal"))
    #right border
    if ball.xcor() < -390:
        ball.goto(0,0)
        ball.dx *=-1
        p2_score = p2_score + 1     #score
        pen.clear()
        pen.write("Player1: "+ str(p1_score) +"  Player2: "+ str(p2_score),align="center",font=("Courier",24,"normal"))
    
    #ball colision with players
    if ball.xcor() > 340 and ball.xcor() < 350 and (ball.ycor() < player2.ycor()+ 40 and ball.ycor() > player2.ycor()- 40 ):
        ball.setx(340)
        ball.dx *=-1
    if ball.xcor() < -340 and ball.xcor() > -350 and (ball.ycor() < player1.ycor()+ 40 and ball.ycor() > player1.ycor()- 40 ):
        ball.setx(-340)
        ball.dx *=-1
