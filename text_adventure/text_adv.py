# ---IulianCenusa---
# ---August 2020---
# ------------------

import random

class Player():

    def __init__(self, hp, name):
        self.name = name  # ---Character name
        self.hp = hp  # ---Hitpoints (life points)

    def heal(self):
        self.hp = self.hp + 1  # ---heal with 1 life point

    def danger(self, damage):
        self.hp = self.hp - damage  # ---get damage

    def get_values(self):
        print("\nYou have "+str(self.hp)+" HP left!\n")


# ---The game---
p_name = input("What is your name? ")
p_hp = random.randint(5, 10)  # ---initial HP
me = Player(p_hp, p_name)

print("Hello little " + me.name + "\nThe adventure beginns")


print("\nYou woke up in a small cabin in the forest with " + str(p_hp) + " HP... After a while, you go outside")
c1 = input("Which road do you choose? (left/right)")
if c1 == "left":
    me.get_values()
    print("\nYou walk on a path... After a while you found a fauntain")
    c2 = input("What do you want to do? Drink from the fauntain or walk away? (drink/walk)")
    if c2 == 'drink':
        me.get_values()
        print("The water is bad and you loose 2 HP")
        me.danger(2)
        print("After you recover from that bad taste, you walk away down on the road you see an axe in a tree")
        c3 = input ("What do you do ? Get the axe or leave it? (get/leave)")
        if c3 =='get':
            me.get_values()
            print("You use the axe to cut some tree branches and find a way out of the forest.\nYOU WON !!!")
        elif c3 =='leave':
            me.get_values()
            print("You venture off in search of the way out but you fell of from hunger")
            me.danger(p_hp)
        else:
            print("Wrong choice... A bear attacks and you die")
            me.danger(p_hp)
    elif c2 == 'walk':
        me.get_values()
        print("You walk away from the fauntain and further on the road you see an axe in a tree")
        c3 = input ("What do you do ? Get the axe or leave it? (get/leave)")
        if c3 =='get':
            me.get_values()
            print("You use the axe to cut some tree branches and find a way out of the forest.\nYOU WON !!!")
        elif c3 =='leave':
            me.get_values()
            print("You venture off in search of the way out but you fell of from thirst")
            me.danger(p_hp)
        else:
            print("Wrong choice... A wolf attacks and you die")
            me.danger(p_hp)
    else:
        print("Wrong choice... You fell in the fauntain and die")
        me.danger(p_hp)

elif c1 == "right":
    me.get_values()
    print("\nYou walk on the right path and arrive at a small lake")
    c2 = input("What do you do? Walk across the lake or swim in it? (swim/across)")
    if c2 == 'swim':
        me.get_values()
        print("The water in teh lake is cold and you loose 5 HP")
        me.danger(5)
    elif c2 == 'across':
        me.get_values()
        print("You found the exit from the forest.\nYOU WON !!!")
    else:
        print("Wrong choice... You fell in a sharp rock and die")
        me.danger(p_hp)
else:
    print("Wrong choice... A monster attacks and you loose your health")
    me.danger(p_hp)
