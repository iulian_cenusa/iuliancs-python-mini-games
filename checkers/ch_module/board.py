import pygame
from .constants import ROWS,COLS,SQ_SIZE, CREAM, BROWN,WHITE,RED
from .piece import Piece

class Board:

    def __init__(self):
        self.board = []
        self.selected_piece = None
        self.red_left = self.white_left = 12
        self.red_kings = self.white_kings = 0
        self.create_board()

    def draw_sq(self,win):
        #draw on a surface (win) a checkers board pattern
        win.fill(BROWN)
        for row in range(ROWS):
            for col in range(row % 2 , ROWS , 2 ): #start, stop, step
                pygame.draw.rect(win,CREAM,( row*SQ_SIZE,col*SQ_SIZE,SQ_SIZE,SQ_SIZE )) #top left and then wifth and height
    
    # white - row 0,1,2 
    # red   - row 5,6,7 

    def create_board(self):
        self.board = [ 
            [0,WHITE,0,WHITE,0,WHITE,0,WHITE],
            [WHITE,0,WHITE,0,WHITE,0,WHITE,0],
            [0,WHITE,0,WHITE,0,WHITE,0,WHITE],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [RED,0,RED,0,RED,0,RED,0],
            [0,RED,0,RED,0,RED,0,RED],
            [RED,0,RED,0,RED,0,RED,0],
        ]

    def draw_pieces(self,win):
        for row in range(ROWS):
            for col in range(COLS):
                if self.board[row][col] != 0:
                    pc = Piece(row,col,self.board[row][col])
                    pc.draw(win)