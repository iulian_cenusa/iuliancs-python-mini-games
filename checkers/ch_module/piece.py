import pygame
from .constants import RED,WHITE,SQ_SIZE,BLACK

class Piece:

    PADDING = 10
    BORDER  = 2

    def __init__(self,row,col,color):
        self.row = row
        self.col = col
        self.color = color
        self.king = False

        if self.color == RED:
            self.direction = -1
        else:
            self.direction = 1
        
        self.x = 0
        self.y = 0
        self.calc_pos()
    
    def calc_pos(self):
        self.x = SQ_SIZE * self.col + SQ_SIZE//2 #center of the square because the piece is a circle
        self.y = SQ_SIZE * self.row + SQ_SIZE//2
    
    def make_king(self):
        self.king = True
    
    def draw(self,win):
        radius = SQ_SIZE//2 - self.PADDING
        pygame.draw.circle( win, BLACK, (self.x,self.y), radius + self.BORDER )
        pygame.draw.circle( win, self.color, (self.x,self.y), radius )

    def __repr__(self):
        return str(self.color)