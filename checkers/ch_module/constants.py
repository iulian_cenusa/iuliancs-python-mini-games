#constant values related to checkers module

import pygame

WIDTH , HEIGHT = 800, 800
ROWS, COLS = 8, 8
SQ_SIZE = WIDTH//COLS

#colors
#RGB
RED = (255,0,0)
WHITE = (255,255,255)
BLACK = (0,0,0)
BLUE = (0,0,255)
CREAM = (230,187,135)
BROWN = (120,73,16)