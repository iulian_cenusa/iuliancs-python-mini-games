import pygame
from ch_module.constants import WIDTH,HEIGHT,RED,WHITE
from ch_module.board import Board
from ch_module.piece import Piece

FPS = 60
WIN = pygame.display.set_mode( (WIDTH,HEIGHT) )
pygame.display.set_caption('Checkers')
board = Board()

def main():
    run = True
    clock = pygame.time.Clock()

    while run:
        clock.tick(FPS)
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
        
        board.draw_sq(WIN) 
        board.draw_pieces(WIN)
        pygame.display.update()
        
    pygame.quit()

main()