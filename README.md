# Iulian Cenusa's Python Mini Games #

## Game list ##

+ Pong (Tutorial)       - finished
+ Snake (Tutorial)      - finished
+ Tic Tac Toe           - unfinished (not in works)
+ Text Adventure Game (Console)   - finished
+ Simple TBS            - in works
+ Simple RPG            - planned
+ Checkers Game (Tutorial)        - in works

## What helped creating these mini games ##

+ [freeCodeCamp's Learn Python by Building Five Games - Full Course](https://www.youtube.com/watch?v=XGf2GcyHPhc)
+ [TechwithTim's Python Project Tutorial - Your First Python Project - just the ideea of a text adventure game](https://www.youtube.com/watch?v=_ZqAVck-WeM)
+ [Piskel App](https://www.piskelapp.com/) - for creating sprites
+ [CHeckers Game Tutorial](https://www.youtube.com/watch?v=vnd3RfeG3NM&lc=z23mflrqrxzisvqp304t1aokgejkvsxqjyecacf5fzwzrk0h00410)
