import pygame
from .constants import SQ_SIZE,ROWS,COLS,WATER,SAND,GRASS,HILL,MOUNTAIN

class Board:

    def __init__(self):
        self.board = []
        self.terain = []
        self.selected_unit = None
        self.create_map()
        self.create_board()

    def create_board(self):
        
        for i in range(ROWS):
            tmp = []
            for j in range(COLS):
                tmp.append(0)
            self.board.append(tmp)

    def create_map(self):
        with open('E:\Projects\mini_games\TBS\\tbs\map.txt') as f: 
            self.terain = [x.strip().split(',') for x in f.readlines()]

    def draw_map(self,win):
        for row in range(COLS):
            for col in range(ROWS):
                if self.terain[col][row] == 'WATER':
                    pygame.draw.rect(win,WATER,( row*SQ_SIZE,col*SQ_SIZE,SQ_SIZE,SQ_SIZE ))
                elif self.terain[col][row] == 'SAND':
                    pygame.draw.rect(win,SAND,( row*SQ_SIZE,col*SQ_SIZE,SQ_SIZE,SQ_SIZE ))
                elif self.terain[col][row] == 'GRASS':
                    pygame.draw.rect(win,GRASS,( row*SQ_SIZE,col*SQ_SIZE,SQ_SIZE,SQ_SIZE ))
                elif self.terain[col][row] == 'HILL':
                    pygame.draw.rect(win,HILL,( row*SQ_SIZE,col*SQ_SIZE,SQ_SIZE,SQ_SIZE ))
                elif self.terain[col][row] == 'MOUNTAIN':
                    pygame.draw.rect(win,MOUNTAIN,( row*SQ_SIZE,col*SQ_SIZE,SQ_SIZE,SQ_SIZE ))
                else:
                    pass

    #---draw grid
    def drawGrid(self,win):
        x = 0
        y = 0
        for l in range(ROWS):
            x = x + SQ_SIZE
            y = y + SQ_SIZE
            pygame.draw.line(win, (0,0,0), (x,0),(x,ROWS*SQ_SIZE))
            pygame.draw.line(win, (0,0,0), (0,y),(COLS*SQ_SIZE,y))