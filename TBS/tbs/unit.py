import pygame

class Unit:

    def __init__(self,name,row,col,color,image,building):
        self.name = name
        self.row = row
        self.col = col
        self.color = color
        self.image = image
        self.building = building

    def draw_unit(self,win):
        win.blit(self.image,(self.row,self.col))
        if self.building:
            pygame.draw.rect(win,self.color,( self.row+8, self.col+4, 5, 2 ))
            pygame.draw.rect(win,self.color,( self.row+25, self.col+4, 5, 2 ))
            pygame.draw.rect(win,self.color,( self.row+12, self.col+19, 2, 3 ))
            pygame.draw.rect(win,self.color,( self.row+18, self.col+19, 2, 3 ))
        else:
            pygame.draw.rect(win,self.color,( self.row+8,self.col+21,16,7 ))
