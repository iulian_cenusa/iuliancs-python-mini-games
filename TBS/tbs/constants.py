#constant values related to turn base game module

import pygame

WIDTH , HEIGHT = 320, 320
SQ_SIZE = 32    # use images of 32x32px
ROWS, COLS = HEIGHT//SQ_SIZE, WIDTH//SQ_SIZE

#colors
#RGB
RED = (255,0,0)
WHITE = (255,255,255)
BLACK = (0,0,0)
BLUE = (0,0,255)
CREAM = (230,187,135)
BROWN = (120,73,16)

#to be used in terain generator
WATER = (99,174,240)
SAND = (255,253,168)
GRASS = (28,253,48)
HILL = (205,186,16)
MOUNTAIN = (128,128,128)