#---TBS Game base on a grid map
#---Author: Iulian Cenusa
#------------------------------

import pygame
from tbs.unit import Unit
from tbs.player import Player 
from tbs.board import Board

from tbs.constants import WIDTH,HEIGHT,ROWS,COLS,SQ_SIZE,RED,BLUE

clock = pygame.time.Clock()
pygame.display.init()

pygame.display.set_caption('IulianC\'s TBS')

screen = pygame.display.set_mode((WIDTH,HEIGHT),0,SQ_SIZE)

fort = pygame.image.load('E:\Projects\mini_games\TBS\images\Fort.png')
inf = pygame.image.load('E:\Projects\mini_games\TBS\images\Inf_sprite.png')

board = Board()
f1 = Unit('Fort_red',SQ_SIZE*2,SQ_SIZE*6,RED,fort,True)
f2 = Unit('Fort_blue',SQ_SIZE*3,SQ_SIZE*6,BLUE,fort,True)
u1 = Unit('Inf_red',SQ_SIZE*6,SQ_SIZE*4,RED,inf,False)
u2 = Unit('Inf_blue',SQ_SIZE*6,SQ_SIZE*3,BLUE,inf,False)

while True:

    screen.fill((255,255,255))
    board.draw_map(screen)
    board.drawGrid(screen)
    f1.draw_unit(screen)
    f2.draw_unit(screen)
    u1.draw_unit(screen)
    u2.draw_unit(screen)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.display.quit()

    pygame.display.update()
    clock.tick(60)
