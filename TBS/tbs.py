#---TBS Game base on a grid map
#---Author: Iulian Cenusa
#------------------------------

import pygame,sys
from pygame.locals import *
from player import Player

#---constants
WINDOW_SIZE = (960,960)     #960x960px screen 
TILE_SIZE = 32              #tiles of 32px

clock = pygame.time.Clock()
pygame.display.init()

pygame.display.set_caption('IulianC\'s TBS')

#---draw grid
def drawGrid(surface):
    x = 0
    y = 0
    for l in range(30):
        if (l <= 24):
            x = x + TILE_SIZE
            y = y + TILE_SIZE
        else:
            x = x + TILE_SIZE
        pygame.draw.line(surface, (0,0,0), (x,0),(x,800))
        pygame.draw.line(surface, (0,0,0), (0,y),(960,y))

screen = pygame.display.set_mode(WINDOW_SIZE,0,32)
fort_red = pygame.image.load('E:/Projects/mini_games/TBS/images/fort_red.png')
fort_blue = pygame.image.load('E:/Projects/mini_games/TBS/images/fort_blue.png')

pl = Player('Player',100,0,[])
ai = Player('AI',100,0,[])
game_turn = 0
player_end = False
ai_end = False 

while True:

    screen.fill((255,255,255))
    screen.blit(fort_red,(32,32))
    screen.blit(fort_blue,(896,736))
    drawGrid(screen)
    print("Game turn: " + str(game_turn))

    if pl.turn == ai.turn and ai.turn > game_turn:
        game_turn += 1

    if pl.turn == game_turn :
        # player moves
        print ("\nPlayer moves\n")
        if player_end:
            pl.end_turn()
            print("Player turn: " + str(pl.turn))
    
    if ai.turn == game_turn :
        # AI moves
        print ("\nAI moves\n")
        if ai_end:
            ai.end_turn()
            print("AI turn: " + str(ai.turn))
    
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.display.quit()
            sys.exit()
        if event.type == KEYDOWN:
            if event.key == K_1:
                player_end = True
            if event.key == K_2:
                ai_end = True
        if event.type == KEYUP:
            if event.key == K_1:
                player_end = False
            if event.key == K_2:
                ai_end = False
    pygame.display.update()
    clock.tick(60)