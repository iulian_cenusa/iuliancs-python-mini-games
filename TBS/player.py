#---Player class

class Player():

    name = ''
    money = 0
    turn = 0
    units = []

    def __init__(self,name,money,turn,units):
        self.name = name
        self.money = money
        self.turn = turn
        self.units = units
    
    def end_turn(self):
        self.turn += 1